﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public class Cryptography
    {
        private static volatile Cryptography instance;

        private static object syncRoot = new Object();

        private Cryptography()
        {





        }
        public static Cryptography Instance
        {
            get
            {
                try
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new Cryptography();
                        }
                        return instance;
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public string getSHA512(string value)
        {
            using (SHA512 shaM = new SHA512Managed())
            {
                byte[] hash = shaM.ComputeHash(Encoding.UTF8.GetBytes(value));
                StringBuilder result = new StringBuilder();
                for (int i = 0; i < hash.Length; i++)
                {
                    result.Append(hash[i].ToString("X2"));
                }
                return result.ToString();
            }
        }
    }

   
}
