﻿using log4net;
using System.Windows;
using Utilities;

namespace QuanLyThuVien
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public MainWindow()
        {
            InitializeComponent();
            log.Debug("HELLO_");
            log.Debug(ConfigCenter.Instance.getConfig("author"));
        }
    }
}
