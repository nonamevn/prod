﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BUS;
using DTO;
using log4net;

namespace QuanLyThuVien
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Window
    {
        public MainMenu()
        {
            InitializeComponent();
        }

		public static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		private void btn_return_books_Click(object sender, RoutedEventArgs e)
		{
			//string id_ = "3";
			//KindDTO id = Kinds_BUS.getKindsID(id_);
			//if (id == null)
			//{
			//	MessageBox.Show("Not exists");
			//}
			//else
			//{
			//	MessageBox.Show("Welcome" + id.name);
			//}
			List<KindDTO> listkind = Kinds_BUS.getListKinds();
			foreach (KindDTO kind in listkind)
			{
				MessageBox.Show(kind.name);
				log.Info(kind.name);
			}
		}
	}
}
