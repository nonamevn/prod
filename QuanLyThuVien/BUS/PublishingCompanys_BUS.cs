﻿using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class PublishingCompanys_BUS
    {
        public static PublishingCompanyDTO getPublishingCompanysID(string id)
        {
            PublishingCompanyDTO e = new DAL_PublishingCompanys().getPublishingCompanysID(id);
            return e;
        }
        public static List<PublishingCompanyDTO> getListPublishingCompanys()
        {
            return DAL_PublishingCompanys.getListPublishingCompanys();
        }
    }
}
