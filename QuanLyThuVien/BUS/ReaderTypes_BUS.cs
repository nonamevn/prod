﻿using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class ReaderTypes_BUS
    {
        public static ReaderTypeDTO getReaderTypesID(string id)
        {
            ReaderTypeDTO e = new DAL_ReaderTypes().getReaderTypesID(id);
            return e;
        }
        public static List<ReaderTypeDTO> getListReaderTypes()
        {
            return DAL_ReaderTypes.getListReaderTypes();
        }
    }
}
