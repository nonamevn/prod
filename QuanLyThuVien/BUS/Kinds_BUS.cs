﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DTO;

namespace BUS
{
	public static class Kinds_BUS
	{
		public static KindDTO getKindsID(string id)
		{
			KindDTO e = new DAL_Kinds().getKindsID(id);
			return e;
		}
		public static List<KindDTO> getListKinds()
		{
			return DAL_Kinds.getListKinds();
		}
	}
}
