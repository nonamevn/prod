﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DTO;

namespace BUS
{
	public static class Divisions_BUS
	{
		public static DivisionDTO getDivisionsID(string id)
		{
			DivisionDTO e = new DAL_Divisions().getDivisionID(id);
			return e;
		}
		public static List<DivisionDTO> getListDivisions()
		{
			return DAL_Divisions.getlistDivisions();
		}
	}
}
