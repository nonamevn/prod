﻿using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public static class Books_Bus
    {
        public static List<tbl_books> getListBooks()
        {
            using(LBM_DOMAIN ctx = new LBM_DOMAIN())
            {
                return ctx.tbl_books.ToList<tbl_books>();
            }
        }
    }
}
