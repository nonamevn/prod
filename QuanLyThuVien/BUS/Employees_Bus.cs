﻿using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace BUS
{
    public class Employees_Bus
    {
        public static EmployeeDTO getEmployeeByUserNameAndPass(string username,string password)
        {
            EmployeeDTO e = new DAL_Employees().getEmployeeByUserNameAndPassword(username, Cryptography.Instance.getSHA512(password));
            e.full_name = "Trinh";
            return e;
        }
		public static List<EmployeeDTO> getListEmployees()
		{
			return DAL_Employees.getListEmployees();
		}
	}
}
