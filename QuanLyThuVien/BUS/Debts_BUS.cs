﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DTO;

namespace BUS
{
	public static class Debts_BUS
	{
		public static DebtDTO getDebtID(string id)
		{
			DebtDTO e = new DAL_Debts().getDebtID(id);
			return e;
		}
		public static List<DebtDTO> getListDegrees()
		{
			return DAL_Debts.getListDebt();
		}
	}
}
