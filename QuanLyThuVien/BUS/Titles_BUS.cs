﻿using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class Titles_BUS
    {
        public static TitleDTO getTitlesID(string id)
        {
            TitleDTO e = new DAL_Titles().getTitlesID(id);
            return e;
        }
        public static List<TitleDTO> getListTitles()
        {
            return DAL_Titles.getListTitles();
        }
    }
}
