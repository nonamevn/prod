﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using DTO;

namespace BUS
{
	public static class Degrees_BUS
	{
		public static DegreeDTO getDegreesID(string id)
		{
			DegreeDTO e = new DAL_Degrees().getDegreeID(id);
			return e;
		}
		public static List<DegreeDTO> getListDegrees()
		{
			return DAL_Degrees.getListDegrees();
		}
	}
}
