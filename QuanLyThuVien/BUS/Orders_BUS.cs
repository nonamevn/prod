﻿using DAL;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public static class Orders_BUS
    {
        public static OrderDTO getOrderID(string id)
        {
            OrderDTO e = new DAL_Orders().getOrderID(id);
            return e;
        }
        public static List<OrderDTO> getListOrders()
        {
            return DAL_Orders.getListOrders();
        }
    }
}
