﻿using log4net;
using System;
using System.Data;
using System.Data.SqlClient;


namespace DAL
{
    public class DBContext
    {

        public static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static volatile DBContext instance;
        private static object syncRoot = new Object();

        private DBContext()
        {

        }

        public static DBContext Instance
        {
            get
            {
                try
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new DBContext();
                        }
                    }
                    return instance;
                }
                catch (Exception e)
                {
                    log.Error(e.Message);
                    return null;
                }
            }
        }

        public SqlDataReader getDataReader(SqlCommand cmd)
        {
            return cmd.ExecuteReader();
        }

        public DataTable getDataTable(SqlCommand cmd)
        {
            try
            {
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
