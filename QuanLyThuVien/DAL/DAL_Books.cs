﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public static class DAL_Books
    {
        public static List<BookDTO> getListBooks()
        {
            string sql = "SELECT * FROM dbo.tbl_books;";
            //GET Connection to DB
            SqlConnection con = DataProvider.Instance.connect();
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;

            try
            {
                SqlDataReader data_reader = DBContext.Instance.getDataReader(cmd);
                List<BookDTO> listResult = new List<BookDTO>();
                while (data_reader.Read())
                {
					BookDTO result = new BookDTO();
                    result.id = long.Parse(data_reader["id"].ToString());
                    result.name = data_reader["name"].ToString();
                    result.is_delete = Boolean.Parse(data_reader["is_delete"].ToString());

                    listResult.Add(result);
                }

                return listResult;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
            }
            return null;
        }
    }
}
