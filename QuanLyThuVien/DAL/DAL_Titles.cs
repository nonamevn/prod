﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAL_Titles
    {
        public class DAL_ReaderTypes
        {
            public TitleDTO getTitlesID(string id)
            {
                string sql = "SELECT * FROM dbo.tbl_titles WHERE id = @id;";
                //GET Connection to DB
                SqlConnection con = DataProvider.Instance.connect();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@id";
                param.SqlDbType = SqlDbType.BigInt;
                param.Direction = ParameterDirection.Input;
                param.Value = id;

                cmd.Parameters.Add(param);
                try
                {
                    SqlDataReader data_reader = DBContext.Instance.getDataReader(cmd);

                    while (data_reader.Read())
                    {
                        TitleDTO result = new TitleDTO();
                        result.id = long.Parse(data_reader["id"].ToString());
                        result.name = data_reader["name"].ToString();
                        return result;
                    }
                }
                catch (Exception e)
                {

                    throw e;
                }
                finally
                {
                    con.Close();
                }
                return null;
            }
            public static List<TitleDTO> getListTitles()
            {
                string sql = "SELECT * FROM dbo.tbl_titles;";
                //GET Connection to DB
                SqlConnection con = DataProvider.Instance.connect();
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.Text;

                try
                {
                    SqlDataReader data_reader = DBContext.Instance.getDataReader(cmd);
                    List<TitleDTO> listResult = new List<TitleDTO>();
                    while (data_reader.Read())
                    {
                        TitleDTO result = new TitleDTO();
                        result.id = long.Parse(data_reader["id"].ToString());
                        result.name = data_reader["name"].ToString();
                        result.is_delete = Boolean.Parse(data_reader["is_delete"].ToString());

                        listResult.Add(result);
                    }

                    return listResult;
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    con.Close();
                }
                return null;
            }
        }

        public static List<TitleDTO> getListTitles()
        {
            throw new NotImplementedException();
        }

        public TitleDTO getTitlesID(string id)
        {
            throw new NotImplementedException();
        }
    }
}
