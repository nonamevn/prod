﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
	public class DAL_Employees
	{

		public EmployeeDTO getEmployeeByUserNameAndPassword(string userName, string passWord)
		{
			string sql = "SELECT * FROM dbo.tbl_employees WHERE user_name = @USERNAME;";
			//GET Connection to DB
			SqlConnection con = DataProvider.Instance.connect();
			SqlCommand cmd = new SqlCommand(sql, con);
			cmd.CommandType = CommandType.Text;

			SqlParameter param = new SqlParameter();
			param.ParameterName = "@USERNAME";
			param.SqlDbType = SqlDbType.NVarChar;
			param.Direction = ParameterDirection.Input;
			param.Value = userName;

			SqlParameter param2 = new SqlParameter();
			param2.ParameterName = "@PASSWORD";
			param2.SqlDbType = SqlDbType.Text;
			param2.Direction = ParameterDirection.Input;
			param2.Value = passWord;

			cmd.Parameters.Add(param);
			cmd.Parameters.Add(param2);
			try
			{
				SqlDataReader data_reader = DBContext.Instance.getDataReader(cmd);

				while (data_reader.Read())
				{
					EmployeeDTO result = new EmployeeDTO();
					result.id = long.Parse(data_reader["id"].ToString());
					result.full_name = data_reader["full_name"].ToString();
					return result;
				}
			}
			catch (Exception e)
			{

				throw e;
			}
			finally
			{
				con.Close();
			}
			return null;
		}
		public static List<EmployeeDTO> getListEmployees()
		{
			string sql = "SELECT * FROM dbo.tbl_employees;";
			//GET Connection to DB
			SqlConnection con = DataProvider.Instance.connect();
			SqlCommand cmd = new SqlCommand(sql, con);
			cmd.CommandType = CommandType.Text;

			try
			{
				SqlDataReader data_reader = DBContext.Instance.getDataReader(cmd);
				List<EmployeeDTO> listResult = new List<EmployeeDTO>();
				while (data_reader.Read())
				{
					EmployeeDTO result = new EmployeeDTO();
					result.id = long.Parse(data_reader["id"].ToString());
					result.full_name = data_reader["full_name"].ToString();
					result.is_delete = Boolean.Parse(data_reader["is_delete"].ToString());

					listResult.Add(result);
				}

				return listResult;
			}
			catch (Exception e)
			{
				throw e;
			}
			finally
			{
				con.Close();
			}
			return null;
		}
	}
}
