﻿using log4net;
using System;
using System.Data.SqlClient;
using Utilities;

namespace DAL
{
    public class DataProvider
    {
        public static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static volatile DataProvider instance;
        private static object syncRoot = new Object();
        private DataProvider()
        {
            log.Debug("initial DAL -> DataProvider");
           
        }

        public static DataProvider Instance
        {
            get
            {
                try
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new DataProvider();
                        }
                    }
                    return instance;
                }
                catch(Exception e)
                {
                    log.Error(e.Message);
                    return null;
                }
            }
        }

        public SqlConnection connect()
        {
            string connectionString = ConfigCenter.Instance.getConfig("sql");
            try
            {
                SqlConnection con =  new SqlConnection(connectionString);
                log.Debug("connect sql success");
                con.Open();
                return con;
            }
            catch (Exception e)
            {
                log.Error(e.Message);
                throw new Exception("Database không tồn tại, vui lòng gọi cho người phụ trách");
            }
        }
    }
}
