﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAL_Orders
    {
        public OrderDTO getOrderID(string id)
        {
            string sql = "SELECT * FROM dbo.tbl_orders WHERE id = @id;";
            //GET Connection to DB
            SqlConnection con = DataProvider.Instance.connect();
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;

            SqlParameter param = new SqlParameter();
            param.ParameterName = "@id";
            param.SqlDbType = SqlDbType.BigInt;
            param.Direction = ParameterDirection.Input;
            param.Value = id;

            cmd.Parameters.Add(param);
            try
            {
                SqlDataReader data_reader = DBContext.Instance.getDataReader(cmd);

                while (data_reader.Read())
                {
                    OrderDTO result = new OrderDTO();
                    result.id = long.Parse(data_reader["id"].ToString());
                    result.reader_id =long.Parse(data_reader["reader_id"].ToString());
                    return result;
                }
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                con.Close();
            }
            return null;
        }
        public static List<OrderDTO> getListOrders()
        {
            string sql = "SELECT * FROM dbo.tbl_orders;";
            //GET Connection to DB
            SqlConnection con = DataProvider.Instance.connect();
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;

            try
            {
                SqlDataReader data_reader = DBContext.Instance.getDataReader(cmd);
                List<OrderDTO> listResult = new List<OrderDTO>();
                while (data_reader.Read())
                {
                    OrderDTO result = new OrderDTO();
                    result.id = long.Parse(data_reader["id"].ToString());
                    result.reader_id = long.Parse(data_reader["reader_id"].ToString());
                    result.is_delete = Boolean.Parse(data_reader["is_delete"].ToString());

                    listResult.Add(result);
                }

                return listResult;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
            }
            return null;
        }
    }
}
