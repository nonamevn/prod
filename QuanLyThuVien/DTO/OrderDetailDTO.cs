﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
	public class OrderDetailDTO
	{
		public long order_id { get; set; } // bigint, not null

		public long book_id { get; set; } // bigint, not null

		public bool is_has_lent { get; set; } // bit, not null

		public long date_of_hire { get; set; } // bigint, not null

		public long expiration_date { get; set; } // bigint, not null
	}
}
