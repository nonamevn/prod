﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
	public class BookReturnDetailDTO
	{
		public long book_return_id { get; set; } // bigint, not null

		public long book_id { get; set; } // bigint, not null

		public long kind_id { get; set; } // bigint, not null

		public long forfeit { get; set; } // bigint, not null
	}
}
