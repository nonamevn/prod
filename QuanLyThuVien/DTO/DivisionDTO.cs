﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
	public class DivisionDTO
	{
		public long id { get; set; } // bigint, not null

		public string name { get; set; } // nvarchar(1024), not null

		public bool is_delete { get; set; } // bit, not null

		public byte[] create_date { get; set; } // timestamp, not null
	}
}
