﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
	public class BookDTO
	{
		public long id { get; set; } // bigint, not null

		public string name { get; set; } // nvarchar(2014), not null

		public long year_create { get; set; } // bigint, not null

		public long publisher_id { get; set; } // bigint, not null

		public long kind_id { get; set; } // bigint, not null

		public long employee_id { get; set; } // bigint, not null

		public byte[] create_date { get; set; } // timestamp, not null

		public decimal price { get; set; } // money, not null

		public bool is_delete { get; set; } // bit, not null
	}
}
