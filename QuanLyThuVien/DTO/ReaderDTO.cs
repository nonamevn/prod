﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
	public class ReaderDTO
	{
		public long id { get; set; } // bigint, not null

		public string full_name { get; set; } // nvarchar(1024), not null

		public long birthday { get; set; } // bigint, not null

		public string address { get; set; } // nvarchar(1024), not null

		public string email { get; set; } // nvarchar(1024), not null

		public byte[] create_date { get; set; } // timestamp, not null

		public long employees_id { get; set; } // bigint, not null

		public long reader_type_id { get; set; } // bigint, not null

		public bool is_delete { get; set; } // bit, not null
	}
}
