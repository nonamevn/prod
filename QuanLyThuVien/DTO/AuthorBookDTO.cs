﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
	public class AuthorBookDTO
	{
		public long author_id { get; set; } // bigint, not null

		public long book_id { get; set; } // bigint, not null
	}
}
