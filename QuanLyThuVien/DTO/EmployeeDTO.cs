﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
	public class EmployeeDTO
	{
		public long id { get; set; } // bigint, not null

		public string full_name { get; set; } // nvarchar(2014), not null

		public long birthday { get; set; } // bigint, not null

		public string address { get; set; } // nvarchar(2014), not null

		public long degree_id { get; set; } // bigint, not null

		public string phone { get; set; } // nvarchar(1), not null

		public long title_id { get; set; } // bigint, not null

		public long division_id { get; set; } // bigint, not null

		public string user_name { get; set; } // nvarchar(1024), not null

		public string password { get; set; } // text, not null

		public byte[] create_date { get; set; } // timestamp, not null

		public bool is_delete { get; set; } // bit, not null

		public bool sex { get; set; } // bit, not null
	}
}
