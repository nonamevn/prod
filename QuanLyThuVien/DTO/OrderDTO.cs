﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
	public class OrderDTO
	{
		public long id { get; set; } // bigint, not null.

		public long reader_id { get; set; } // bigint, not null.

		public byte[] create_date { get; set; } // timestamp, not null.

		public long employee_id { get; set; } // bigint, not null.

		public bool is_delete { get; set; } // bit, not null.
	}
}
